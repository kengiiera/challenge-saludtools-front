import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { ColorPickerModule } from 'primeng/colorpicker';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DialogModule } from 'primeng/dialog';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MenubarModule } from 'primeng/menubar';
import { PanelModule } from 'primeng/panel';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { RatingModule } from 'primeng/rating';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { ToolbarModule } from 'primeng/toolbar';
import { MenuModule } from '../menu/menu.module';
import {InputNumberModule} from 'primeng/inputnumber';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import {ConfirmationService} from 'primeng/api';
import { PatientComponent } from './components/patient.component';

const routes: Routes = [{ path: '', component: PatientComponent }];

@NgModule({
  declarations: [PatientComponent],
  bootstrap: [PatientComponent],
  imports: [
    ColorPickerModule,
    BrowserModule,
    MenuModule,
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    ScrollingModule,
    InputTextModule,
    ConfirmPopupModule,
    MenubarModule,
    BrowserAnimationsModule,
    PanelModule,
    InputNumberModule,
    CardModule,
    DropdownModule,
    TableModule,
    DialogModule,
    InputTextareaModule,
    ContextMenuModule,
    CalendarModule,
    ProgressSpinnerModule,
    ButtonModule,
    ToastModule,
    ToolbarModule,
    RatingModule,
    ConfirmDialogModule,
    InputSwitchModule,
    FormsModule,
    ReactiveFormsModule,
    MenuModule,
    TabViewModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [MessageService, ConfirmationService]
})
export class PatientModule { }
