import { Patient } from "./Patient";

export class DrugPrescription {
    public id!: number | null;
    public name!: string;
    public patient!: any;
    public drugId!: number;

  }