export class Patient {
  public id!: number;
  public name?: string;
  public surname!: string;
  public gender!: string;
  public dateBirth!: Date;
  public status!: boolean;
  public age!: number;
public numberPrescription!: number;
}
