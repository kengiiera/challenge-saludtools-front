export class Drug {
  public id!: number;
  public name!: string;
  public min_age!: number;
  public max_age!: number;
  public single_gender!: string;
}
