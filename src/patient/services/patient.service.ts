import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { Response } from '../../shared/model/Response';
import { ResponsePage } from '../../shared/model/ResponsePage';
import { constantsUrlApi } from '../../shared/constants/ConstantsUrlApi';
import { Patient } from '../models/Patient';
const options = {
  observe: 'body' as const,
  responseType: 'json' as const,
};

@Injectable({
  providedIn: 'root',
})
export class PatientService {
  constructor(private http: HttpClient) {}

  async getAllPatientsActives(): Promise<Response> {
    return await lastValueFrom(
      this.http.get<ResponsePage>(`${constantsUrlApi.PATIENT_API}`, options)
    );
  }

  async savePatient(patient: Patient): Promise<Response> {
    return await lastValueFrom(
      this.http.post<Response>(
        `${constantsUrlApi.PATIENT_API}`,
        patient,
        options
      )
    );
  }

  async deletePatient(patient: Patient): Promise<Response> {
    return await lastValueFrom(
      this.http.delete<Response>(`${constantsUrlApi.PATIENT_API}/${patient.id}`)
    );
  }
}
