import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { Response } from '../../../shared/model/Response';
import { constantsUrlApi } from '../../../shared/constants/ConstantsUrlApi';
import { DrugPrescription } from '../../../patient/models/DrugPrescription';
import { ResponsePage } from 'src/shared/model/ResponsePage';
const options = {
  observe: 'body' as const,
  responseType: 'json' as const,
};

@Injectable({
  providedIn: 'root',
})
export class DrugPrescriptionService {
  constructor(private http: HttpClient) {}

  async getDrugsPatientByPatientId(patientId: number): Promise<Response> {
    return await lastValueFrom(
      this.http.get<ResponsePage>(
        `${constantsUrlApi.DRUG_PRESCRIPTION_API}/${patientId}`,
        options
      )
    );
  }

  async saveDrugPrescription(
    drugPrescription: DrugPrescription
  ): Promise<Response> {
    return await lastValueFrom(
      this.http.post<Response>(
        `${constantsUrlApi.DRUG_PRESCRIPTION_API}`,
        drugPrescription,
        options
      )
    );
  }
  async validateNumberPrescriptionByDrug(
    patiendId: number,
    drugId: number
  ): Promise<Response> {
    return await lastValueFrom(
      this.http.get<Response>(
        `${constantsUrlApi.DRUG_PRESCRIPTION_API}/validateNumberPrescriptionByDrug/${patiendId}/${drugId}`,
        options
      )
    );
  }
}
