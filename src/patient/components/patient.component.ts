import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { Constants } from '../../shared/constants/Constants';
import { Patient } from '../models/Patient';
import { PatientService } from '../services/patient.service';
import { ConfirmationService } from 'primeng/api';
import { DrugPrescriptionService } from '../services/drugPrescription/drugPrescription.service';
import { DrugPrescription } from '../models/DrugPrescription';
import { Drug } from '../models/Drug';
import drugsData from '../../shared/constants/Drugs.json';
interface Gender {
  name: string;
}
@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html'
})
export class PatientComponent implements OnInit {
  formPatient: FormGroup = new FormGroup({});
  formDrugPrescription: FormGroup = new FormGroup({});
  isEditing!: boolean;
  patientDialog!: boolean;
  prescriptionDialog!: boolean;
  drugPrescriptionDialogCreate!: boolean;
  busqueda!: string;
  active!: boolean;
  patients!: Patient[];
  genders!: Gender[];
  displaySaveDialog!: boolean;
  progressSpinnerDlg = false;
  maxDate!: Date;
  @ViewChild('dt')
  table!: Table;
  paginatorValue!: boolean;
  rowsPerPage!: number[];
  responsiveLayout!: string;
  templateValue!: string;
  numberPrescriptionDrug!: number;
  canPrescripted!: boolean;
  drugsPrescriptions!: DrugPrescription[];
  drugs!: Drug[];
  constructor(
    private patientService: PatientService,
    private drugPrescriptionService: DrugPrescriptionService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {
    this.formPatient = new FormGroup({
      id: new FormControl(''),
      name: new FormControl('', Validators.required),
      dateBirth: new FormControl(Validators.required),
      surname: new FormControl('', Validators.required),
      gender: new FormControl(Validators.required),
      status: new FormControl(true),
    });

    this.formDrugPrescription = new FormGroup({
      id: new FormControl(''),
      drugId: new FormControl(Validators.required),
      patientId: new FormControl(Validators.required),
    });
  }

  async ngOnInit() {
    this.progressSpinnerDlg = true;
    this.paginatorValue = Constants.PAGINATOR_VALUE;
    this.rowsPerPage = Constants.ROWS_PER_PAGE;
    this.responsiveLayout = Constants.RESPONSIVE_LAYOUT_VALUE;
    this.templateValue = Constants.TEMPLATE;
    this.patients = (
      await this.patientService.getAllPatientsActives()
    ).result.content;
    this.progressSpinnerDlg = false;
    this.genders = [{ name: 'Hombre' }, { name: 'Mujer' }];
    this.maxDate = new Date();
    this.canPrescripted = true;
  }

  hideDialog() {
    this.patientDialog = false;
    this.isEditing = false;
    this.formPatient.reset();
  }

  hideDialogPrescription() {
    this.drugPrescriptionDialogCreate = false;
    this.formDrugPrescription.reset();
  }
  async submitPatient() {
    this.progressSpinnerDlg = true;
    if (this.formPatient.valid) {
      const patient = this.formPatient.value;
      await this.patientService.savePatient(patient).then((value) => {
        if (value.status === 200) {
          const patientSave = value.result;
          this.updatePatientList(patientSave);
          this.messageService.add({
            severity: 'success',
            life: Constants.LIFE_TIME_ALERTS,
            summary: 'Exitoso',
            detail: value.message,
          });
        } else {
          this.messageService.add({
            severity: 'error',
            life: Constants.LIFE_TIME_ALERTS,
            summary: 'Error',
            detail: value.message,
          });
        }
      });
      this.patientDialog = false;
      this.formPatient.reset();
    } else {
      this.formPatient.markAllAsTouched();
      this.messageService.add({
        severity: 'error',
        life: Constants.LIFE_TIME_ALERTS,
        summary: 'Por favor revise la información',
        detail: 'Errores en el formulario',
      });
    }
    this.progressSpinnerDlg = false;
  }

  updatePatientList(patient: Patient) {
    const updated = this.patients
      .filter((e) => e.id === patient.id)
      .map((f) => f == patient);

    if (updated.length === 0) this.patients.push(patient);
    else {
      this.findIndexById(patient);
    }
  }

  createForm() {
    this.progressSpinnerDlg = true;
    this.formPatient = new FormGroup({
      id: new FormControl(),

      name: new FormControl('', [
        Validators.required,
        Validators.minLength(Constants.MIN_LENGTH_INPUT_NAME_PATIENT),
        Validators.maxLength(Constants.MAX_LENGTH_INPUT_NAME_PATIENT),
      ]),

      surname: new FormControl('', [
        Validators.required,
        Validators.minLength(Constants.MIN_LENGTH_INPUT_NAME_PATIENT),
        Validators.maxLength(Constants.MAX_LENGTH_INPUT_NAME_PATIENT),
      ]),
      gender: new FormControl('', Validators.required),
      dateBirth: new FormControl('', Validators.required),
      status: new FormControl(true),
    });

    this.patientDialog = true;
    this.isEditing = false;
    this.progressSpinnerDlg = false;
  }

  editForm(patient: Patient) {
    this.progressSpinnerDlg = true;
    this.formPatient = new FormGroup({
      name: new FormControl(patient.name, [
        Validators.required,
        Validators.minLength(Constants.MIN_LENGTH_INPUT_NAME_PATIENT),
        Validators.maxLength(Constants.MAX_LENGTH_INPUT_NAME_PATIENT),
      ]),

      surname: new FormControl(patient.surname, [
        Validators.required,
        Validators.minLength(Constants.MIN_LENGTH_INPUT_NAME_PATIENT),
        Validators.maxLength(Constants.MAX_LENGTH_INPUT_NAME_PATIENT),
      ]),
      gender: new FormControl(patient.gender, Validators.required),
      dateBirth: new FormControl(patient.dateBirth, Validators.required),
      id: new FormControl(patient.id),
      status: new FormControl(patient.status),
    });
    this.patientDialog = true;
    this.isEditing = true;
    this.progressSpinnerDlg = false;
  }

  confirm(event: Event, patient: Patient) {
    this.confirmationService.confirm({
      target: event.target as EventTarget,
      message: '¿Está seguro de eliminar el paciente seleccionado?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.deletePatient(patient);
      },
      reject: () => {},
    });
  }

  async deletePatient(patient: Patient) {
    const result = await this.patientService.deletePatient(patient);

    if (result.status === 200) {
      this.deleteIndexById(patient);
      this.messageService.add({
        severity: 'success',
        life: Constants.LIFE_TIME_ALERTS,
        summary: result.message,
        detail: 'Paciente eliminado correctamente',
      });
    } else {
      this.messageService.add({
        severity: 'error',
        life: Constants.LIFE_TIME_ALERTS,
        summary: result.message,
        detail: 'Error',
      });
    }
  }
  findIndexById(patient: Patient) {
    let index = -1;
    for (let i = 0; i < this.patients.length; i++) {
      if (this.patients[i].id === patient.id) {
        index = i;
        break;
      }
    }

    this.patients[index] = patient;
  }

  clear(table: Table) {
    this.busqueda = '';
    table.clear();
  }

  deleteIndexById(patient: Patient) {
    let index = -1;
    for (let i = 0; i < this.patients.length; i++) {
      if (this.patients[i].id === patient.id) {
        index = i;
        break;
      }
    }
    this.patients.splice(index, 1);
  }

  async showPrescriptions(patient: Patient) {
    this.progressSpinnerDlg = true;
    this.drugsPrescriptions = (
      await this.drugPrescriptionService.getDrugsPatientByPatientId(patient.id)
    ).result.content;
    this.progressSpinnerDlg = false;
    this.prescriptionDialog = true;
  }

  createFormDrugPrescription(patient: Patient) {
    let gender = patient.gender === 'Mujer' ? 'FEMALE' : 'MALE';
    this.progressSpinnerDlg = true;
    this.drugs = drugsData;

    this.drugs = this.drugs.filter(
      (x) => x.single_gender === gender || x.single_gender === null
    );

    this.drugs = this.drugs.filter(
      (x) =>
        (x.min_age <= patient.age || x.min_age === null) &&
        (x.max_age >= patient.age || x.max_age === null)
    );
    this.formDrugPrescription = new FormGroup({
      id: new FormControl(),
      patientId: new FormControl(patient.id, Validators.required),
      drugId: new FormControl('',  [
        Validators.required]),
    });

    this.drugPrescriptionDialogCreate = true;
    this.progressSpinnerDlg = false;
  }

  async submitDrugPrescription() {
    this.progressSpinnerDlg = true;
    if (this.formDrugPrescription.valid) {
      const drugPrescription = this.formDrugPrescription.value;
      const drugPrescriptionValue: DrugPrescription = {
        id: null,
        drugId: drugPrescription.drugId.id,
        name: drugPrescription.drugId.name,
        patient: { id: drugPrescription.patientId },
      };
      await this.drugPrescriptionService
        .saveDrugPrescription(drugPrescriptionValue)
        .then((value) => {
          this.updateNumberPrescriptions(drugPrescription.patientId);
          if (value.status === 200) {
            this.messageService.add({
              severity: 'success',
              life: Constants.LIFE_TIME_ALERTS,
              summary: 'Exitoso',
              detail: value.message,
            });
          } else {
            this.messageService.add({
              severity: 'error',
              life: Constants.LIFE_TIME_ALERTS,
              summary: 'Error',
              detail: value.message,
            });
          }
        });
      this.drugPrescriptionDialogCreate = false;
      this.formDrugPrescription.reset();
    } else {
      this.formDrugPrescription.markAllAsTouched();
      this.messageService.add({
        severity: 'error',
        life: Constants.LIFE_TIME_ALERTS,
        summary: 'Por favor revise la información',
        detail: 'Errores en el formulario',
      });
    }
    this.progressSpinnerDlg = false;
  }

  updateNumberPrescriptions(patientId: number) {
    for (let i = 0; i < this.patients.length; i++) {
      if (this.patients[i].id === patientId) {
        this.patients[i].numberPrescription =
          this.patients[i].numberPrescription + 1;
        break;
      }
    }
  }
  async changeOption() {
    this.canPrescripted = true;
    let drugId = this.formDrugPrescription.controls['drugId'].value;
    this.numberPrescriptionDrug = (
      await this.drugPrescriptionService.validateNumberPrescriptionByDrug(
        this.formDrugPrescription.controls['patientId'].value,
        drugId.id
      )
    ).result;
    if (this.numberPrescriptionDrug > 0) {
      this.canPrescripted = false;
      this.messageService.add({
        severity: 'error',
        life: Constants.LIFE_TIME_ALERTS,
        summary: '',
        detail:
          'Este medicamento ya fue prescrito a este paciente en el mes actual',
      });
    }
  }
}
