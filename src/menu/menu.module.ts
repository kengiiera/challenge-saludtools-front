import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InputTextModule } from 'primeng/inputtext';
import { MenubarModule } from 'primeng/menubar';
import { TabViewModule } from 'primeng/tabview';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { TooltipModule } from 'primeng/tooltip';
import { MessageModule } from 'primeng/message';
import { MenuComponent } from './components/menu.component';
import { Constants } from '../../src/shared/constants/Constants';
import { PatientComponent } from '../patient/components/patient.component';

const routes: Routes = [
  { path: 'menu', component: MenuComponent },
  {
    path: 'patient',
    component: PatientComponent,
    data: {
      breadcrumb: {
        alias: 'Pacientes',
        label: 'Pacientes',
        info: Constants.ICON_HOME
      }
    }
  }
];

@NgModule({
  declarations: [MenuComponent],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    HttpClientModule,
    ButtonModule,
    MenubarModule,
    MessageModule,
    TieredMenuModule,
    TooltipModule,
    InputTextModule,
    TabViewModule,
    RouterModule.forChild(routes)
  ],
  exports: [MenuComponent],
  providers: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MenuModule {}
