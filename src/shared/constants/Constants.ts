export class Constants {
  static TRANSLATION_CONFIG = {
    clear: 'Limpiar',
    apply: 'Aplicar',
    accept: 'Aceptar',
    reject: 'Cancelar',
    emptyMessage: 'No se encontraron registros',
    emptyFilterMessage: 'No se encontraron registros'
  };

  static ICON_HOME = 'pi pi-home';

  static LIFE_TIME_ALERTS = 20000;

  static RESPONSIVE_LAYOUT_VALUE = 'scroll';

  static PAGINATOR_VALUE = true;

  static ROWS_PER_PAGE = [10, 25, 30, 50, 100];

  static TEMPLATE = 'Mostrando {first} a {last} de {totalRecords} registros';


  static MIN_LENGTH_INPUT_NAME_PATIENT= 1;

  static MAX_LENGTH_INPUT_NAME_PATIENT= 99;

}
