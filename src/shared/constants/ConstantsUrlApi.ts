import { environment } from '../../environments/environment';

export const constantsUrlApi = {
  PATIENT_API: `${environment.apiUrl}/patient`,
  DRUG_PRESCRIPTION_API: `${environment.apiUrl}/drug-prescription`,
};
